package com.atguigu.gmallpublisher;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @Author lzc
 * @Date 2020/10/17 10:01
 */
public class A {
    public static void main(String[] args) {
        System.out.println(LocalDate.parse("2020-09-01").plusDays(-1).toString());
        System.out.println(LocalDateTime.now().toString());
    }
}
