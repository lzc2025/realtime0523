package com.atguigu.gmallpublisher.controller;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmallpublisher.bean.Option;
import com.atguigu.gmallpublisher.bean.SaleInfo;
import com.atguigu.gmallpublisher.bean.Stat;
import com.atguigu.gmallpublisher.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author lzc
 * @Date 2020/10/16 16:39
 */
@RestController
public class PublisherController {

    @Autowired
    PublisherService service;

    @GetMapping("/realtime-total")
    public String realtimeTotal(String date) {
        Long total = service.getDau(date);

        List<Map<String, Object>> result = new ArrayList<>();

        Map<String, Object> map1 = new HashMap<>();
        map1.put("id", "dau");
        map1.put("name", "新增日活");
        map1.put("value", total);
        result.add(map1);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("id", "new_mid");
        map2.put("name", "新增设备");
        map2.put("value", 233);
        result.add(map2);


        Map<String, Object> map3 = new HashMap<>();
        map3.put("id", "order_amount");
        map3.put("name", "新增交易额");
        map3.put("value", service.getTotalAmount(date));
        result.add(map3);

        return JSON.toJSONString(result);
    }

    @GetMapping("/realtime-hour")
    public String realtimeHour(String id, String date) {
        if ("dau".equals(id)) {
            Map<String, Long> today = service.getHourDau(date);
            Map<String, Long> yesterday = service.getHourDau(getYesterday(date));

            Map<String, Map<String, Long>> result = new HashMap<>();
            result.put("today", today);
            result.put("yesterday", yesterday);
            return JSON.toJSONString(result);
        } else if ("order_amount".equals(id)) {
            Map<String, BigDecimal> today = service.getHourTotalAmount(date);
            Map<String, BigDecimal> yesterday = service.getHourTotalAmount(getYesterday(date));

            Map<String, Map<String, BigDecimal>> result = new HashMap<>();
            result.put("today", today);
            result.put("yesterday", yesterday);
            return JSON.toJSONString(result);
        }

        return "";
    }


    @GetMapping("/sale_detail")
    public String saleDetail(String date, int startpage, int size, String keyword) throws IOException {
        Map<String, Object> saleDetailAndAgg = service.getSaleDetailAndAgg(date, keyword, startpage, size);
        Long total = (Long) saleDetailAndAgg.get("total");
        List<HashMap> detail = (List<HashMap>) saleDetailAndAgg.get("details");
        Map<String, Long> ageAgg = (Map<String, Long>) saleDetailAndAgg.get("ageAgg");
        Map<String, Long> genderAgg = (Map<String, Long>) saleDetailAndAgg.get("genderAgg");

        SaleInfo saleInfo = new SaleInfo();
        // 1. 设置总数
        saleInfo.setTotal(total);
        // 2. 设置详情
        saleInfo.setDetail(detail);
        // 3. 这种饼图
        // 3.1 设置性别饼图
        Stat genderStat = new Stat();
        genderStat.setTitle("用户性别占比");
        for (Map.Entry<String, Long> entry : genderAgg.entrySet()) {
            String gender = entry.getKey().equals("M") ? "男" : "女";
            Long count = entry.getValue();
            Option option = new Option(gender, count);
            genderStat.addOption(option);
        }
        saleInfo.addStat(genderStat);
        // 3.2 设置年龄饼图
        Stat ageStat = new Stat();
        ageStat.setTitle("用户年龄占比");
        ageStat.addOption(new Option("20岁以下", 0L));
        ageStat.addOption(new Option("20岁到30岁", 0L));
        ageStat.addOption(new Option("30岁以上", 0L));
        for (String age : ageAgg.keySet()) {
            int a = Integer.parseInt(age);
            Long count = ageAgg.get(age);
            if (a < 20) {
                Option opt = ageStat.getOptions().get(0);
                opt.setValue(opt.getValue() + count);
            } else if (a < 30) {
                Option opt = ageStat.getOptions().get(1);
                opt.setValue(opt.getValue() + count);
            }else {
                Option opt = ageStat.getOptions().get(2);
                opt.setValue(opt.getValue() + count);
            }
        }
        saleInfo.addStat(ageStat);

        return JSON.toJSONString(saleInfo);
    }

    /**
     * 计算昨天的年月日
     *
     * @param date
     * @return
     */
    private String getYesterday(String date) {
        return LocalDate.parse(date).plusDays(-1).toString();
    }
}
