package com.atguigu.gmallpublisher.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author lzc
 * @Date 2020/10/24 11:13
 */
public class Stat {
   private String title;
   private List<Option> options = new ArrayList<>();

   public void addOption(Option option){
       options.add(option);
   }

    public Stat(String title) {
        this.title = title;
    }

    public Stat() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Option> getOptions() {
        return options;
    }

}
