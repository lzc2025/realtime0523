package com.atguigu.gmallpublisher.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Author lzc
 * @Date 2020/10/24 11:19
 */
public class SaleInfo {
    private Long total;
    private List<HashMap> detail;
    private List<Stat> stats = new ArrayList<>();

    public SaleInfo() {
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<HashMap> getDetail() {
        return detail;
    }

    public void setDetail(List<HashMap> detail) {
        this.detail = detail;
    }

    public List<Stat> getStats() {
        return stats;
    }

    public void addStat(Stat stat) {
        this.stats.add(stat);
    }
}
