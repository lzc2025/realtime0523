package com.atguigu.gmallpublisher.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

public interface PublisherService {
    Long getDau(String date);

    Map<String, Long> getHourDau(String date);

    BigDecimal getTotalAmount(String date);
    Map<String, BigDecimal> getHourTotalAmount(String date);
    /*
    总数
    聚合结果
    详情
    Map["total"->62, "detail"-> List<Map>, "genderAggs"-> .. "ageAggs"->...]
     */
    Map<String, Object> getSaleDetailAndAgg(String date,
                             String keyword,
                             int startpage,
                             int size) throws IOException;

}

/*
List<Map<String, Object>
    List(Map("loghour"->10, count->100), Map(....))
        +----------+--------+
    | LOGHOUR  | COUNT  |
    +----------+--------+
    | 14       | 182    |
    | 15       | 106    |
    | 17       | 11     |
    +----------+--------+

Map("10"->100, "11"->200, ...)

 */