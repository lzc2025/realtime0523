package com.atguigu.gmallpublisher.mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Author lzc
 * @Date 2020/10/19 14:16
 */
public interface OrderMapper {
    BigDecimal getTotalAmount(String date);

    List<Map<String, Object>> getHourTotalAmount(String date);

}
