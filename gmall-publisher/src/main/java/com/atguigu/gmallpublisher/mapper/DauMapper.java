package com.atguigu.gmallpublisher.mapper;

import java.util.List;
import java.util.Map;

public interface DauMapper {
    Long getDau(String date);

    // 获取每个小时的日活明细
    List<Map<String, Object>> getHourDau(String date);

}
/*
+----------+--------+
| LOGHOUR  | COUNT  |
+----------+--------+
| 14       | 182    |
| 15       | 106    |
| 17       | 11     |
+----------+--------+



 */