package com.atguigu.realtime.app

import com.atguigu.realtime.util.MyKafkaUtil
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}

abstract class BaseApp {
    val appName: String
    val master: String
    val batchTime: Int
    val groupId: String
    val topic: String
    
    def main(args: Array[String]): Unit = {
        val conf = new SparkConf().setAppName(appName).setMaster(master)
        val ssc: StreamingContext = new StreamingContext(conf, Seconds(batchTime))
        val sourceStream: DStream[String] = MyKafkaUtil.getKafkaStream(ssc, groupId, topic)
        // 具体的业务
        run(ssc, sourceStream)
        
        ssc.start()
        ssc.awaitTermination()
    }
    
    def run(ssc: StreamingContext, sourceStream: DStream[String])
}
