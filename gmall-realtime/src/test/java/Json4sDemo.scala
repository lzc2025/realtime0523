import com.alibaba.fastjson.JSON
import org.json4s.jackson.{JsonMethods, Serialization}

import scala.beans.BeanProperty

/**
 * Author atguigu
 * Date 2020/10/19 10:19
 */
object Json4sDemo {
    def main(args: Array[String]): Unit = {
        /*val a: A = A(10, "zs")
        val r: String = JSON.toJSONString(a, true)
        println(r)*/
        
//        val json =
//            """
//              |{"name": "zs", "age": 20}
//              |""".stripMargin
//        implicit val f = org.json4s.DefaultFormats
//        /*println((JsonMethods.parse(json) \ "name").extract[String])
//        println((JsonMethods.parse(json) \ "age").extract[Int])*/
//        val user = JsonMethods.parse(json).extract[User]
//        println(user)
        
//        val user = User(10, "lisi")
//        val user = Map("a" -> 97, "b" -> 98)
        val user = List(30, 50, 70, 60, 10, 20)
        implicit val f = org.json4s.DefaultFormats
        val s: String = Serialization.write(user)
        println(s)
        
        
    }
}

case class User(age: Int, name: String)
/*
json操作分两种:
 1. 解析
        反序列化
        json => 对象
        
        在scala中, 用fastjson解析一般问题大
 2. 序列化
        对象=> json字符串
        
        在scala中, 用fastjson序列化, 做不到

 */
