/**
 * Author atguigu
 * Date 2020/10/23 11:34
 */
object MapFlatDemo {
    def main(args: Array[String]): Unit = {
        /*val list1 = List(30, 5, 7, 6, 10, 2)
        val list2 = list1.flatMap(x => {
            if(x %2 == 0){
                Array(x, x*x, x * x *x)
            }else{
                Array[Int]()
            }
        })
    
        println(list2)*/
        
        val s = "abc"
        println(s.flatMap(x => x + "abc"))
    }
}
