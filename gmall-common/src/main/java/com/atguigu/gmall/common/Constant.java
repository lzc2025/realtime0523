package com.atguigu.gmall.common;

/**
 * @Author lzc
 * @Date 2020/10/14 10:38
 */
public class Constant {
    // shift + ctrl + u
    public static final String STARTUP_TOPIC = "startup_topic";
    public static final String EVENT_TOPIC = "event_topic";

    public static final String ORDER_INFO_TOPIC = "order_info_topic";
    public static final String ORDER_DETAIL_TOPIC = "order_detail_topic";
}
