package com.atguigu

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

/**
 * Author atguigu
 * Date 2020/10/17 16:50
 */
object MyKafkaUtil {
    
    val props = new Properties()
    props.put("bootstrap.servers", "hadoop102:9092,hadoop103:9092,hadoop104:9092")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    // 创建一个生产者
    private val producer = new KafkaProducer[String, String](props)
    
    def sendToKafka(topic: String, content: String): Unit ={
        producer.send(new ProducerRecord[String, String](topic, content))
    }
    
}
